//
//  TelaDiasTableViewController.h
//  AppTempo
//
//  Created by Eric Mesquita on 15/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface TelaDiasTableViewController : UITableViewController <CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}
@end
