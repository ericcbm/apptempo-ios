//
//  AppDelegate.h
//  AppTempo
//
//  Created by Eric Mesquita on 14/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableDictionary *appTempo;

- (void) saveAppTempo;

@end

