//
//  TelaDiasTableViewController.m
//  AppTempo
//
//  Created by Eric Mesquita on 15/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "TelaDiasTableViewController.h"
#import "TelaDetalhesViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"

@interface TelaDiasTableViewController ()

@property (strong, nonatomic) NSArray * forecasts;
@property (strong, nonatomic) NSDictionary *local;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property int timesLocationUpdated;

@end

@implementation TelaDiasTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Puxe para Atualizar"];
    [refresh addTarget:self action:@selector(startUpdatingLocation)
     
      forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewWillAppear:(BOOL)animated {
    self.title = @"";
    self.forecasts = [NSArray array];
    [self.tableView reloadData];
    
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    self.timesLocationUpdated = 0;
    [self startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Location Manager

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined && [manager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [manager requestAlwaysAuthorization];
    }
}

// Failed to get current location
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar sua localização." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    self.title = @"";
    self.forecasts = [NSArray array];
    [self.tableView reloadData];
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    [self performSelector:@selector(stopRefresh)];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
//    NSDate* eventDate = newLocation.timestamp;
//    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
//    if (abs(howRecent) < 15.0) {
//        // If the event is recent, do something with it.
//        NSLog(@"latitude %+.6f, longitude %+.6f\n",
//              newLocation.coordinate.latitude,
//              newLocation.coordinate.longitude);
//    }
    NSLog(@"%d", self.timesLocationUpdated);
    if (self.timesLocationUpdated == 0) {
        self.timesLocationUpdated++;
    } else {
        self.timesLocationUpdated = 0;
            [self updateUILocationFromLat:newLocation.coordinate.latitude lon:newLocation.coordinate.longitude];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.forecasts) {
        return self.forecasts.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellDia" forIndexPath:indexPath];
    
    NSMutableString *dt = [NSMutableString string];
    
    if (indexPath.row == 0) {
        [dt appendString:@"Hoje, "];
    } else if (indexPath.row == 1) {
        [dt appendString:@"Amanhã, "];
    }
    [dt appendString:[self stringDateFormatedFromUnixTimeInteger:[[self.forecasts[indexPath.row] objectForKey:@"dt"] doubleValue]]];
    cell.textLabel.text = dt;
    
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    NSString *unit = [[delegate.appTempo objectForKey:@"unit"] isEqual:@"metric"] ? @"°C": @"°F";
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Max: %d%@ / Min: %d%@", [[[self.forecasts[indexPath.row] objectForKey:@"temp"] objectForKey:@"max"] integerValue], unit, [[[self.forecasts[indexPath.row] objectForKey:@"temp"] objectForKey:@"min"] integerValue], unit];
    
    NSArray *list = [self.forecasts[indexPath.row] objectForKey:@"weather"];
    UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png", [list[0] objectForKey:@"icon"]]];
    [cell.imageView setImage:image];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"mostrarDetalhes"]) {
        TelaDetalhesViewController * controller = segue.destinationViewController;
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        controller.forecast = self.forecasts[indexPath.row];
        controller.title = cell.textLabel.text;
    }
}

#pragma mark - Internal Methods

- (void)startUpdatingLocation
{
    self.local = nil;
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    NSMutableArray *locals = [delegate.appTempo objectForKey:@"locals"];
    for (NSMutableDictionary* local in locals) {
        if ([[local valueForKey:@"active"] boolValue]) {
            self.local = local;
            break;
        }
    }
    if (!self.local) {
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
        }
        [locationManager startUpdatingLocation];
    } else {
        [self updateUILocationFromLat:[[self.local objectForKey:@"lat"] doubleValue] lon:[[self.local objectForKey:@"lon"] doubleValue]];
    }
}

- (void)stopUpdatingLocation
{
    if (locationManager)
        [locationManager stopUpdatingLocation];
}

- (void) updateUILocationFromLat:(double)lat lon:(double) lon {
    NSLog(@"latitude %+.6f, longitude %+.6f\n",
          lat,
          lon);
    [self stopUpdatingLocation];
    AFHTTPRequestOperationManager *afManager = [AFHTTPRequestOperationManager manager];
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithDouble:lat], @"lat",
                            [NSNumber numberWithDouble:lon], @"lon",
                            [delegate.appTempo objectForKey:@"unit"], @"units",
                            [NSNumber numberWithInt:16], @"cnt",
                            @"d03b465103f2447375f9abbf6737a576", @"APPID",
                            nil];
    [afManager GET:@"http://api.openweathermap.org/data/2.5/forecast/daily" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject objectForKey:@"cod"] isEqualToString:@"200"]) {
            self.title = self.local ? [self.local objectForKey:@"name"] : [[[responseObject objectForKey:@"city"] objectForKey:@"name"] isEqual:@""] ? [[responseObject objectForKey:@"city"] objectForKey:@"country"] : [[responseObject objectForKey:@"city"] objectForKey:@"name"];
            self.forecasts = [NSArray arrayWithArray:[responseObject objectForKey:@"list"]];
            [self.tableView reloadData];
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            
            NSMutableDictionary *sectionLocal = [NSMutableDictionary dictionary];
            [sectionLocal setObject:self.title forKey:@"name"];
            [sectionLocal setObject:[NSNumber numberWithDouble:lat] forKey:@"lat"];
            [sectionLocal setObject:[NSNumber numberWithDouble:lon] forKey:@"lon"];
            [sectionLocal setValue:[NSNumber numberWithBool:YES] forKey:@"active"];
            [[NSUserDefaults standardUserDefaults] setObject:sectionLocal forKey:@"local"];
            
        } else {
            NSLog(@"Error");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar previsões." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar previsões." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"Error: %@", error);
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
    }];
    [self performSelector:@selector(stopRefresh)];
}

- (void)stopRefresh
{
    [self.refreshControl endRefreshing];
}

-(NSString *) stringDateFormatedFromUnixTimeInteger:(double)value {
    NSTimeInterval _interval = value;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    NSLocale *portugueseBrazil = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [formatter setTimeZone:gmt];
    [formatter setLocale:portugueseBrazil];
    [formatter setDateFormat:@"dd 'de' MMMM"];
    return [formatter stringFromDate:date];
}

@end
