//
//  TelaMapaViewController.m
//  AppTempo
//
//  Created by Usuario Curso de IOS on 30/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "TelaMapaViewController.h"
#import "TelaDiasTableViewController.h"

@interface TelaMapaViewController ()

@end

@implementation TelaMapaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mapView setShowsUserLocation:YES];
    self.mapView.delegate = self;
    

    
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    NSDictionary *local = [[NSUserDefaults standardUserDefaults] objectForKey:@"local"];
    
    CLLocationCoordinate2D annotationCoord;
    annotationCoord.latitude = [[local objectForKey:@"lat"] doubleValue];
    annotationCoord.longitude = [[local objectForKey:@"lon"] doubleValue];
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = [local objectForKey:@"name"];
    
    [self.mapView removeAnnotations:[self.mapView annotations]];
    [self.mapView addAnnotation:annotationPoint];
    [self.mapView selectAnnotation:annotationPoint animated:YES];
    [self.mapView setCenterCoordinate:annotationPoint.coordinate animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation: (MKUserLocation *)userLocation
{
    self.mapView.centerCoordinate =
    userLocation.location.coordinate;
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = userLocation.location.coordinate;
    //annotationPoint.title = userLocation.location.;
    
    [mapView addAnnotation:annotationPoint];
    [mapView selectAnnotation:annotationPoint animated:YES];
}
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
