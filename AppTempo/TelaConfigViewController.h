//
//  TelaConfigViewController.h
//  AppTempo
//
//  Created by Usuario Curso de IOS on 23/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelaConfigViewController : UIViewController <UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource>

@end
