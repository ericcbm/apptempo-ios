//
//  TelaDetalhesViewController.m
//  AppTempo
//
//  Created by Eric Mesquita on 16/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "TelaDetalhesViewController.h"
#import "AppDelegate.h"

@interface TelaDetalhesViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelMax;
@property (weak, nonatomic) IBOutlet UILabel *labelMin;
@property (weak, nonatomic) IBOutlet UILabel *labelHumidade;
@property (weak, nonatomic) IBOutlet UILabel *labelPressao;
@property (weak, nonatomic) IBOutlet UILabel *labelVento;
@property (weak, nonatomic) IBOutlet UIImageView *imgClima;
@property (weak, nonatomic) IBOutlet UILabel *labelImgDescricao;

@end

@implementation TelaDetalhesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.forecast) {
        AppDelegate * delegate = [UIApplication sharedApplication].delegate;
        NSString *unit = [[delegate.appTempo objectForKey:@"unit"] isEqual:@"metric"] ? @"°C": @"°F";
        self.labelMax.text = [NSString stringWithFormat:@"%d%@", [[[self.forecast objectForKey:@"temp"] objectForKey:@"max"] integerValue], unit];
        self.labelMin.text = [NSString stringWithFormat:@"%d%@", [[[self.forecast objectForKey:@"temp"] objectForKey:@"min"] integerValue], unit];
        self.labelHumidade.text = [NSString stringWithFormat:@"Humidade: %@%%", [[self.forecast objectForKey:@"humidity"] stringValue]];
        self.labelPressao.text = [NSString stringWithFormat:@"Pressão: %5.2f hPa", [[self.forecast objectForKey:@"pressure"] doubleValue]];
        self.labelVento.text = [NSString stringWithFormat:@"Vento: %5.2f km/h %@", [[self.forecast objectForKey:@"speed"] doubleValue], [self windDirectionStringFromFloat:[[self.forecast objectForKey:@"deg"] doubleValue]]];
        
        NSArray *list = [self.forecast objectForKey:@"weather"];
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"%@-big.png", [list[0] objectForKey:@"icon"]]];
        [self.imgClima setImage:image];
        self.labelImgDescricao.text = [self weatherStringPtBrFromString:[list[0] objectForKey:@"main"]];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSString *) weatherStringPtBrFromString:(NSString *)value {
    if ([value isEqualToString:@"Tempestade"]) {
        return @"etc etc";
    } else if ([value isEqualToString:@"Drizzle"]) {
        return @"Neblina";
    } else if ([value isEqualToString:@"Rain"]) {
        return @"Chuva";
    } else if ([value isEqualToString:@"Snow"]) {
        return @"Neve";
    } else if ([value isEqualToString:@"Clouds"]) {
        return @"Nuvens";
    } else if ([value isEqualToString:@"Clear"]) {
        return @"Céu Aberto";
    } else {
        return value;
    }
}

//http://climate.umn.edu/snow_fence/Components/winddirectionanddegreeswithouttable3.htm
- (NSString *) windDirectionStringFromFloat:(float)direction {
    if (direction > 348.75 && direction <= 11.25) {
        return @"N";
    } else if (direction > 11.25 && direction <= 33.75) {
        return @"NNE";
    } else if (direction > 33.75 && direction <= 56.25) {
        return @"NE";
    } else if (direction > 56.25 && direction <= 78.75) {
        return @"ENE";
    } else if (direction > 78.75 && direction <= 101.25) {
        return @"E";
    } else if (direction > 101.25 && direction <= 123.75) {
        return @"ESE";
    } else if (direction > 123.75 && direction <= 146.25) {
        return @"SE";
    } else if (direction > 146.25 && direction <= 168.75) {
        return @"SSE";
    } else if (direction > 168.75 && direction <= 191.25) {
        return @"S";
    } else if (direction > 191.25 && direction <= 213.75) {
        return @"SSW";
    } else if (direction > 213.75 && direction <= 236.25) {
        return @"SW";
    } else if (direction > 236.25 && direction <= 258.75) {
        return @"WSW";
    } else if (direction > 258.75 && direction <= 281.25) {
        return @"W";
    } else if (direction > 281.25 && direction <= 303.75) {
        return @"WNW";
    } else if (direction > 303.75 && direction <= 326.25) {
        return @"NW";
    } else if (direction > 326.25 && direction <= 348.75) {
        return @"NNW";
    } else {
        return @"";
    }
}

@end
