//
//  AppDelegate.m
//  AppTempo
//
//  Created by Eric Mesquita on 14/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void) saveAppTempo
{
    if(![self.appTempo writeToFile:[self plistPath] atomically:NO]) {
        NSLog(@"Ocorreu um erro durante gravacao no plist");
    }
}

- (void) loadAppTempo
{
    self.appTempo = [NSMutableDictionary dictionaryWithContentsOfFile:[self plistPath]];
    
    //Se o plist esta vazio, cria um novo array
    if(!self.appTempo) {
        NSLog(@"Criou um array novo");
        self.appTempo = [NSMutableDictionary dictionary];
        [self.appTempo setObject:@"metric" forKey:@"unit"];
        [self.appTempo setObject:[NSMutableArray array] forKey:@"locals"];
        [self saveAppTempo];
        
    } else {
        NSLog(@"Carregou o plist com sucesso!");
    }
}

- (NSString *) plistPath
{
    //Pasta destino do simulador: ~/Library/Application Support/iPhone Simulator/
    NSArray * pListpaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,  YES);
    
    //~/Library/Application Support/iPhone Simulator/
    NSString * pListdocumentsDirectory = [pListpaths objectAtIndex:0];
    
    //~/Library/Application Support/iPhone Simulator/Checklists.plist
    NSString * pListpath = [pListdocumentsDirectory stringByAppendingPathComponent:@"AppTempo.plist"];
    
    NSLog(@"%@", pListpath);
    return pListpath;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //    NSLog([self plistPath]);
    [self loadAppTempo];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
