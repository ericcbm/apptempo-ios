//
//  TelaMapaViewController.h
//  AppTempo
//
//  Created by Usuario Curso de IOS on 30/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TelaMapaViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
