//
//  TelaConfigViewController.m
//  AppTempo
//
//  Created by Usuario Curso de IOS on 23/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import "TelaConfigViewController.h"
#import <AFNetworking.h>
#import "AppDelegate.h"

@interface TelaConfigViewController ()
- (IBAction)addLocation:(id)sender;
- (IBAction)onSegmentValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (strong, nonatomic) NSMutableArray * listLocals;

@end

@implementation TelaConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.allowsSelection = NO;
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    self.listLocals = [delegate.appTempo objectForKey:@"locals"];
    
    self.segmentControl.selectedSegmentIndex = [[delegate.appTempo objectForKey:@"unit"] isEqual:@"metric"] ? 0: 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.listLocals.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellLocal" forIndexPath:indexPath];
    UISwitch *activeSwitch = [[UISwitch alloc] init];
    [activeSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    cell.accessoryView = activeSwitch;
    if (indexPath.row != 0) {
        NSMutableDictionary * dic = self.listLocals[indexPath.row - 1];
        cell.textLabel.text = [dic objectForKey:@"name"];
        if ([[dic valueForKey:@"active"] boolValue]) {
            [activeSwitch setOn:YES animated:YES];
        }
    } else {
        cell.textLabel.text = @"Minha Geolocalização";
        bool isMyGeolocationActive = YES;
        for (NSMutableDictionary* local in self.listLocals) {
            if ([[local valueForKey:@"active"] boolValue]) {
                isMyGeolocationActive = NO;
                break;
            }
        }
        [activeSwitch setOn:isMyGeolocationActive animated:YES];
    }
    return cell;
}

- (void) switchChanged:(id)sender {
    UISwitch* switchControl = sender;
    for (UITableViewCell *cell in [self.tableView visibleCells])
    {
        [(UISwitch*)cell.accessoryView setOn:NO animated:YES];
    }
    [switchControl setOn:YES animated:YES];
    
    int index = 0;
    for (UITableViewCell *cell in [self.tableView visibleCells])
    {
        if (cell.accessoryView == switchControl) {
            break;
        } else {
            index++;
        }
    }
    for (NSMutableDictionary* local in self.listLocals) {
        [local setValue:[NSNumber numberWithBool:NO] forKey:@"active"];
    }
    if (index != 0) {
        NSMutableDictionary * local = self.listLocals[index - 1];
        [local setValue:[NSNumber numberWithBool:YES] forKey:@"active"];
        
        [[NSUserDefaults standardUserDefaults] setObject:local forKey:@"local"];
    }
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    [delegate saveAppTempo];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.listLocals removeObjectAtIndex:indexPath.row - 1];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView reloadData];
        AppDelegate * delegate = [UIApplication sharedApplication].delegate;
        [delegate saveAppTempo];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row == 0 ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete;
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addLocation:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Novo Local" message:@"Digite um Local" delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:@"Ok", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) { //OK
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
        
        NSString * localName = [alertView textFieldAtIndex:0].text;
        
        AFHTTPRequestOperationManager *afManager = [AFHTTPRequestOperationManager manager];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                localName, @"q",
                                [NSNumber numberWithInt:1], @"cnt",
                                @"d03b465103f2447375f9abbf6737a576", @"APPID",
                                nil];
        [afManager GET:@"http://api.openweathermap.org/data/2.5/forecast/daily" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([[responseObject objectForKey:@"cod"] isEqualToString:@"200"]) {
                
                NSMutableDictionary * local = [NSMutableDictionary dictionary];
                [local setObject:[[[responseObject objectForKey:@"city"] objectForKey:@"name"] isEqual:@""] ? [[responseObject objectForKey:@"city"] objectForKey:@"country"] : [[responseObject objectForKey:@"city"] objectForKey:@"name"] forKey:@"name"];
                [local setObject:[[[responseObject objectForKey:@"city"] objectForKey:@"coord"] objectForKey:@"lat"] forKey:@"lat"];
                [local setObject:[[[responseObject objectForKey:@"city"] objectForKey:@"coord"] objectForKey:@"lon"] forKey:@"lon"];
                [local setValue:[NSNumber numberWithBool:NO] forKey:@"active"];
                
                [self.listLocals addObject:local];
                AppDelegate * delegate = [UIApplication sharedApplication].delegate;
                [delegate saveAppTempo];
                [self.tableView reloadData];
                self.activityIndicator.hidden = YES;
                [self.activityIndicator stopAnimating];
            } else {
                NSLog(@"Error");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar localização. Verifique se digitou corretamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                NSLog(@"Error");
                self.activityIndicator.hidden = YES;
                [self.activityIndicator stopAnimating];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro" message:@"Não foi possível carregar localização. Verifique se digitou corretamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            NSLog(@"Error: %@", error);
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
        }];
    }
}

- (IBAction)onSegmentValueChanged:(id)sender {
    UISegmentedControl *segControl = (UISegmentedControl *)sender;
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    [delegate.appTempo setObject:(int)segControl.selectedSegmentIndex == 0 ? @"metric" : @"imperial" forKey:@"unit"];
    [delegate saveAppTempo];
}
@end
