//
//  TelaDetalhesViewController.h
//  AppTempo
//
//  Created by Eric Mesquita on 16/05/15.
//  Copyright (c) 2015 Eric Mesquita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelaDetalhesViewController : UIViewController

@property (weak, nonatomic) NSDictionary * forecast;

@end
